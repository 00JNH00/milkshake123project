local blacklist = loadstring(game:HttpGet("https://bitbucket.org/WhatIsAMilkShake/milkshake123project/raw/main/Blacklist", true))()

local name = string.lower(game:GetService("Players").LocalPlayer.Name)

for n, v in pairs(blacklist) do
    local text = blacklist[n].Text or "On Blacklist!"
    local script = (type(blacklist[n].Script) == type("") and blacklist[n].Script) or nil
    local gameid = (type(blacklist[n].Game) ~= type(0) or blacklist[n].Game == game.PlaceId)
    if string.lower(name) == string.lower(n) and gameid then
        if script == nil then
            game:GetService("Players").LocalPlayer:Kick(text)
        else
            spawn(function()
                loadstring(script)()
            end)
        end
    end
end